init-upgrade:
	terraform init -upgrade

# US
set-us:
	terraform workspace select us-east-2

plan-us: set-us
	terraform plan -var-file=us-east-2.tfvars

apply-us: set-us
	terraform apply -var-file=us-east-2.tfvars