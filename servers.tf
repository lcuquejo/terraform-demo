module "multi_server" {
    source = "./modules/multi_server"
    my_ami = "ami-089fe97bc00bff7cc"
    public_subnet_id = module.vpc.public_subnets[0]
    private_subnet_id = module.vpc.private_subnets[0]
    environment_name = var.environment_name
    key_name=aws_key_pair.deployer.key_name
    sg_pub_id=aws_security_group.allow_ssh_pub.id
    sg_priv_id=aws_security_group.allow_ssh_priv.id
}

resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDvPUZk+MOMT+MP/M3M2qqd8RXRsedfNd0bn0Gh/r94ArQfes18NJdo0rN50o6//x6OTrTLh8vn1DzALQ9w0c8LNz/QFMzIkwgcsaWqUU26wS266PkNyZTsmXrRipTeBD68oQDLosAibT/k2QE8YEeb8PXSOlYe53xVj7mS53QnnUAs/RRESLHDnoHJ/nfXEAo28SqAKgefpvhwNMFzZBY10uL9NBXz1l7fidHdJCxBUiYRMwv0dShcI2TpiyesOw6IyfN5kLCfnCenB9n4yeEMXc2AWdAijaa70eH+fptiP2DQ6uOitkz+OvVUp73HuuthLok4lkwcTpn/1TNo/TkMFWQqW5vG1bN9nO2s9aomg/to91MzPnGt81rB0RZ2MxHVix11NcfTPrPwUO7vLgQY/K7K1QaYMpwXnjXqdzuVGm99NyByO1bBOpmVIIDwqzTSuzyr5TtS9am6v6qeMskt323kc2yjMetT02xcdnAQysKgrFlN19bXlLpxS44MWrCG1ZaYN0C29//rzwFVBTxvaYUAOxYzxnoKRNc88daBSR0MefgxrT/x4o2ms8A7qqmoXbH4cC0uvKfUd2Nd0N2Y4OW3fem/tqtNfbycuwj/c7spwr9S+qsGfa417kKQit4/3cLUhCIV3aTtzJrrXpGg0d9HW+rHbMRSNOtLJOgggw=="
}

output "backend_ip" {
  value = module.multi_server.backend_ip
}
output "lb_ip" {
  value = module.multi_server.lb_ip
}
