resource "aws_s3_bucket" "assets" {
  bucket = "${var.environment_name}-assets"

  tags = {
    Name        = "${var.environment_name}-assets"
    Environment = "${var.environment_name}"
  }
}

resource "aws_s3_bucket_acl" "assets" {
  bucket = aws_s3_bucket.assets.id

  acl = "public-read"
}