# Terraform Quick Demo

Just a quick demo, using terraform workspaces and built a NOT very useful module :)

Also not using remote state and count for creating aws_instances, it's just a quick demo.

To run this you can run terraform commands directly or use make:
```
make plan-us
```
```
make apply-us
```

I'm using make ([Makefile](Makefile)) to be able to deploy in multiple regions easily.

Also, there is a pipeline that you can see here: <https://gitlab.com/lcuquejo/terraform-demo/-/pipelines>, that will check for syntax errors in this terraform repository.