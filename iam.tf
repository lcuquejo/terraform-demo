data "aws_iam_policy_document" "list_s3" {
  statement {
    actions   = ["s3:ListAllMyBuckets"]
    resources = ["arn:aws:s3:::*"]
    effect = "Allow"
  }
  statement {
    actions   = ["s3:*"]
    resources = [aws_s3_bucket.assets.arn]
    effect = "Allow"
  }
}

resource "aws_iam_policy" "s3_policy" {
  name        = "${var.environment_name}-policy"
  description = "List"
  policy = data.aws_iam_policy_document.list_s3.json
}

# Allow airportlocker to access s3 new bucket
data "aws_iam_user" "my_existent_user" {
  user_name = "someuser"
}

resource "aws_iam_user_policy_attachment" "attachment" {
  user       = data.aws_iam_user.my_existent_user.user_name
  policy_arn = aws_iam_policy.s3_policy.arn
}

output "aws_iam_policy_document" {
    value =  resource.aws_iam_policy.s3_policy.arn
}