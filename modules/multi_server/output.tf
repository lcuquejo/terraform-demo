output "backend_ip" {
  value = aws_instance.backend_server.private_ip
}

output "lb_ip" {
  value = aws_instance.loadbalancer_server.public_ip
}

output "frontend_ip" {
  value = aws_instance.frontend_server.public_ip
}