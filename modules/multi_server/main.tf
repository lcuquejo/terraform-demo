resource "aws_instance" "frontend_server" {
  ami           = var.my_ami
  instance_type = "t2.micro"
  subnet_id     = var.private_subnet_id
  key_name      = var.key_name
  vpc_security_group_ids      = [var.sg_priv_id]

  root_block_device {
    volume_size = 10
    volume_type = "gp3"
  }
  tags = {
    Name = "${var.environment_name}-myFrontEnd"
  }
}
resource "aws_instance" "backend_server" {
  ami           = var.my_ami
  instance_type = "t2.micro"
  subnet_id     = var.private_subnet_id
  key_name      = var.key_name
  vpc_security_group_ids      = [var.sg_priv_id]

  root_block_device {
    volume_size = 10
    volume_type = "gp3"
  }
  tags = {
    Name = "${var.environment_name}-MyBackEnd"
  }
}

resource "aws_instance" "loadbalancer_server" {
  ami           = var.my_ami
  instance_type = "t2.micro"
  subnet_id     = var.public_subnet_id
  key_name      = var.key_name
  vpc_security_group_ids      = [ var.sg_pub_id, var.sg_priv_id ]

  root_block_device {
    volume_size = 10
    volume_type = "gp3"
  }
  tags = {
    Name = "${var.environment_name}-MyLB"
  }
}
